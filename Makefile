
all : /usr/sbin/autobackup /usr/sbin/autobackup_test

/usr/sbin/autobackup : src/usr/sbin/autobackup /etc/autobackup.conf /etc/logrotate.d/autobackup
	cp "${<}" "${@}"
	git add "${<}"

/usr/sbin/autobackup_test : test/usr/sbin/autobackup_test /etc/autobackup.conf
	cp "${<}" "${@}"
	git add "${<}"

/etc/autobackup.conf : src/etc/autobackup.conf
	cp "${<}" "${@}"
	git add "${<}"

/etc/logrotate.d/autobackup : src/etc/logrotate.d/autobackup
	cp "${<}" "${@}"
	git add "${<}"

