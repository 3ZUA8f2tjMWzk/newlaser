#!/bin/bash

cryptsetup luksOpen /dev/sda5 crypt_root

vgchange --activate y ubuntu-vg
lvchange --activate y ubuntu-vg/root
lvreduce --verbose --size 8G --resizefs ubuntu-vg/root

