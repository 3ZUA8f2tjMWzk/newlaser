#!/bin/bash

function move_it ( )
{
    lvcreate --size 8G --name ${1} ubuntu-vg
    mkfs.ext4 /dev/ubuntu-vg/${1}
    mkdir --parents /mnt/${1}
    chmod ${3} /mnt/${1}
    mount /dev/ubuntu-vg/${1} /mnt/${1}
    chmod ${3} /mnt/${1}
    if [ -d /${2} ]
    then
    	mv /${2}/* /mnt/${1}
    else
	mkdir --parents /${2}
    fi
    mount /${2}
    chmod ${3} /${2}
}

mkdir --parents /snapshot/{minute,hour,day,week,month}

move_it tmp tmp 1777
move_it vartmp var/tmp 1777
move_it varlog var/log 0755
move_it varautobackupminute var/autobackup/minute 0700
move_it varautobackuphour var/autobackup/hour 0700
move_it varautobackupday var/autobackup/day 0700
move_it varautobackupweek var/autobackup/week 0700
move_it varautobackupmonth var/autobackup/month 0700
move_it varautobackuptar var/autobackup/tar 0700
move_it home home 0755
