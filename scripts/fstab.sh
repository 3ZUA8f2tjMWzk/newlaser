#!/bin/bash

echo /dev/mapper/ubuntu--vg-home                /home                                ext4            defaults,nosuid,nodev,noexec,quota   1 2 >> /etc/fstab
echo /dev/mapper/ubuntu--vg-tmp                 /tmp                                 ext4            defaults,nosuid,nodev,noexec         1 2 >> /etc/fstab
echo /dev/mapper/ubuntu--vg-vartmp              /var/tmp                             ext4            defaults,nosuid,nodev,noexec         1 2 >> /etc/fstab
echo /dev/mapper/ubuntu--vg-varlog              /var/log                             ext4            defaults,nosuid,nodev,noexec         1 2 >> /etc/fstab
echo /dev/mapper/ubuntu--vg-varautobackupminute /var/autobackup/minute               ext4            defaults,nosuid,nodev,noexec         1 2 >> /etc/fstab
echo /dev/mapper/ubuntu--vg-varautobackuphour   /var/autobackup/hour                 ext4            defaults,nosuid,nodev,noexec         1 2 >> /etc/fstab
echo /dev/mapper/ubuntu--vg-varautobackupday    /var/autobackup/day                  ext4            defaults,nosuid,nodev,noexec         1 2 >> /etc/fstab
echo /dev/mapper/ubuntu--vg-varautobackupweek   /var/autobackup/week                 ext4            defaults,nosuid,nodev,noexec         1 2 >> /etc/fstab
echo /dev/mapper/ubuntu--vg-varautobackupmonth  /var/autobackup/month                ext4            defaults,nosuid,nodev,noexec         1 2 >> /etc/fstab
echo /dev/mapper/ubuntu--vg-varautobackuptar    /var/autobackup/tar                  ext4            defaults,nosuid,nodev,noexec         1 2 >> /etc/fstab
echo /var/autobackup/minute                     /snapshot/minute                     none            remount,ro                           1 2 >> /etc/fstab
echo /var/autobackup/hour                       /snapshot/hour                       none            remount,ro                           1 2 >> /etc/fstab
echo /var/autobackup/day                        /snapshot/day                        none            remount,ro                           1 2 >> /etc/fstab
echo /var/autobackup/week                       /snapshot/week                       none            remount,ro                           1 2 >> /etc/fstab
echo /var/autobackup/month                      /snapshot/month                      none            remount,ro                           1 2 >> /etc/fstab


